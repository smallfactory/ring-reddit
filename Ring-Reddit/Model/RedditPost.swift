//
//  RedditPost.swift
//  Ring-Reddit
//
//  Created by Small Factory Studios on 10/18/17.
//  Copyright © 2017 SmallFactoryStudios. All rights reserved.
//

import UIKit

struct PostsWrapper: Decodable {
    let kind: String
    let data: Data
}

struct Data: Decodable {
    let modhash: String
    let children: [Children]
    let after: String
}

struct Children: Decodable {
    let kind: String
    let data: RedditPost
}

struct Source: Decodable {
    let url: String
    let width: Int
    let height: Int
}

struct PreviewImage: Decodable {
    let source: Source
}

struct Preview: Decodable {
    let images:[PreviewImage]
}

struct RedditPost: Decodable {
    let title: String
    let author: String
    let thumbnail: String
    let created: Int
    let num_comments: Int
    let preview: Preview?
}
