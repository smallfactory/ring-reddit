//
//  MainController.swift
//  Ring-Reddit
//
//  Created by Small Factory Studios on 10/18/17.
//  Copyright © 2017 SmallFactoryStudios. All rights reserved.
//

import UIKit

class MainController: UICollectionViewController {

    private let mainCellId = "mainCellId"
    private var hasLoadedBottomHalf = false
    private var afterValue:String?
    
    var posts = [Children]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = SFSStyles.color.greenWhite.value
        collectionView?.register(MainCell.self, forCellWithReuseIdentifier: mainCellId)
        
        setupNavigationItems()
        fetchPosts(url: "https://www.reddit.com/top.json")
    }
    
    private func setupNavigationItems() {
        navigationItem.title = "Reddit Top 50"
    }
    
    private func fetchPosts(url: String){
        ApiService.instance.fetchPosts(url: url) { posts in
            
            if self.afterValue != nil {
                self.posts = self.posts + posts.data.children
            } else {
                self.posts = posts.data.children
                self.afterValue = posts.data.after
            }
            
            self.collectionView?.reloadData()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.item == self.posts.count - 3 {
            
            if !hasLoadedBottomHalf {
                hasLoadedBottomHalf = true
                guard let afterValue = afterValue else { return }
                let botomHalf = "https://www.reddit.com/top.json?after=\(String(describing: afterValue))&count=25"
                fetchPosts(url: botomHalf)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let largePhoto = self.posts[indexPath.item].data.preview?.images[0].source.url, largePhoto.contains(".jpg") || largePhoto.contains(".png") else { return }
        
        let vc = PhotoController()
        vc.setLargePhoto(url: largePhoto)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainCellId, for: indexPath) as! MainCell
        cell.post = posts[indexPath.item].data
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }

}

extension MainController: UICollectionViewDelegateFlowLayout {
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
