//
//  PhotoController.swift
//  Ring-Reddit
//
//  Created by Small Factory Studios on 10/21/17.
//  Copyright © 2017 SmallFactoryStudios. All rights reserved.
//

import UIKit
import SnapKit
import SwiftHEXColors

class PhotoController: UIViewController {
    private var largePhotoURL: String?
    
    private let photoHolder: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        return iv
    }()
    
    private let bgHolder: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationItems()
        createUI()
    }
    
    func setLargePhoto(url: String) {
        photoHolder.loadImage(urlString: url)
    }
    
    private func setupNavigationItems() {
        navigationController?.navigationBar.tintColor = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save Photo", style: .done, target: self, action: #selector(saveToPhotoLibrary))
    }
    
    private func createUI() {
        view.addSubview(bgHolder)
        
        bgHolder.snp.makeConstraints { make in
            make.edges.equalTo(0)
        }
        
        view.addSubview(photoHolder)

        photoHolder.snp.makeConstraints { make in
            make.edges.equalTo(0)
        }
    }

    @objc func saveToPhotoLibrary() {
        
        guard let imageToSave = photoHolder.image,
        let imageData = UIImageJPEGRepresentation(imageToSave, 1.0),
        let photo = UIImage(data: imageData)  else { return }
        
        UIImageWriteToSavedPhotosAlbum(photo, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
}

extension PhotoController: UIImagePickerControllerDelegate {

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
}
