//
//  APIService.swift
//  Ring-Reddit
//
//  Created by Small Factory Studios on 10/18/17.
//  Copyright © 2017 SmallFactoryStudios. All rights reserved.
//

import Foundation

struct ApiService {
    
    static let instance = ApiService()
    
    func fetchPosts(url: String, completion: @escaping (PostsWrapper) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                debugPrint("Error")
                return
            }
            
            guard let data = data else { return }
            
            do {
                let posts = try JSONDecoder().decode(PostsWrapper.self, from: data)
                
                DispatchQueue.main.sync {
                    completion(posts)
                }
            } catch let jsonError {
                debugPrint("jsonError : ", jsonError)
            }
        }).resume()
    }
}
