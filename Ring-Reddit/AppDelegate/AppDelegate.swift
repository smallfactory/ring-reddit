//
//  AppDelegate.swift
//  Ring-Reddit
//
//  Created by Small Factory Studios on 10/18/17.
//  Copyright © 2017 SmallFactoryStudios. All rights reserved.
//

import UIKit

    @UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate {
        
        var window: UIWindow?
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainNav = UINavigationController()
        let mainVC = MainController(collectionViewLayout: UICollectionViewFlowLayout())
        mainNav.viewControllers = [mainVC]
        
        window!.rootViewController = mainNav
        window?.makeKeyAndVisible()
        
        return true
    }
}
