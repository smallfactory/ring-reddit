//
//  MainCell.swift
//  Ring-Reddit
//
//  Created by Small Factory Studios on 10/18/17.
//  Copyright © 2017 SmallFactoryStudios. All rights reserved.
//

import UIKit
import SnapKit

class MainCell: UICollectionViewCell {
    
    private var hasThumbnail: Bool = false
    
    private var topLabelConstraint: Constraint? = nil
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 4
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        
        return label
    }()
    
    private let captionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 1
        return label
    }()
    
    private let thumbnailImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        
        return iv
    }()
    
    private let commentsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    var post: RedditPost? {
        didSet {
            
            if let thumbImg = post?.thumbnail, thumbImg.contains(".jpg") || thumbImg.contains(".png"){
                thumbnailImageView.loadImage(urlString: thumbImg)
                hasThumbnail = true
            }
            
            titleLabel.text = post?.title
            setupAttributedCaption()
            setupComments()
            setNeedsUpdateConstraints()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        addSubview(thumbnailImageView)
        thumbnailImageView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.trailing.equalTo(0)
            make.height.equalTo(120)
            make.width.equalTo(120)
        }
        
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(10)
            make.leading.equalTo(10)
            make.trailing.equalTo(-10)
        }
        
        addSubview(commentsLabel)
        commentsLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.equalTo(10)
            make.trailing.equalTo(-10)
            make.height.equalTo(18)
        }
        
        addSubview(captionLabel)
        captionLabel.snp.makeConstraints { make in
            make.top.equalTo(thumbnailImageView.snp.bottom)
            make.leading.equalTo(10)
            make.height.equalTo(18)
        }
    }
    
    override func updateConstraints() {
        if hasThumbnail == true {
            titleLabel.snp.updateConstraints({ make in
                make.top.equalTo(10)
                make.leading.equalTo(10)
                make.trailing.equalTo(-140)
            })
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupComments() {
        guard let post = self.post else { return }
        let commentsStr = post.num_comments == 1 ? "comment" : "comments"
        
        let attributedText = NSMutableAttributedString(string: "\(post.num_comments) \(commentsStr)", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.lightGray,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)
        ])
        
        commentsLabel.attributedText = attributedText
    }
    
    private func setupAttributedCaption() {
        
        guard let post = self.post else { return }
        
        let timeCreated = Date(timeIntervalSince1970: TimeInterval(post.created)).timeAgoDisplay()
        
        let attributedText = NSMutableAttributedString(string: "submitted \(timeCreated )", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.lightGray,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)
        ])
        
        attributedText.append(NSAttributedString(string: " by \(post.author)", attributes: [
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)
            ]))

        captionLabel.attributedText = attributedText
    }
}
